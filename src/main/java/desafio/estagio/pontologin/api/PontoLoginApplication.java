package desafio.estagio.pontologin.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PontoLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(PontoLoginApplication.class, args);
	}

}
